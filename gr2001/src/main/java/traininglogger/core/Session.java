package traininglogger.core;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Session implements Iterable<Exercise> {

    private String date;
    private List<Exercise> exercises = new ArrayList<Exercise>();

    public Session() { // Når de lages for første gang
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        LocalDateTime date = LocalDateTime.now();
        String dateAsString = date.format(formatter);
        this.date = dateAsString;
    }

    public Session(String date){ // Når de rekonstrueres fra json-fil
        this.date = date;
    }

    public String getDate() {
        return this.date;
    }

    public void addExercise(Exercise exercise) {
        this.exercises.add(exercise);
    }

    @Override
    public Iterator<Exercise> iterator() {
        return this.exercises.iterator();
    }
}