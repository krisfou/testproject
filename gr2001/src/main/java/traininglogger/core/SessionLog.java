package traininglogger.core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SessionLog implements Iterable<Session> {

    private List<Session> sessions = new ArrayList<Session>();

    @Override
    public Iterator<Session> iterator() {
        return this.sessions.iterator();
    }

    public void addSession(Session session){
        this.sessions.add(session);
    }
}