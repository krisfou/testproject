package traininglogger.core;

public class Set {

    private double weight;
    private int reps;

    public Set(double weight, int reps){
        this.weight = weight; // Bør være mer enn 0, men hvem skal ta seg av det?
        this.reps = reps; // Bør være mer enn 0, men hvem skal ta seg av det?
    }

    public double getWeight(){
        return this.weight;
    }

    public int getReps(){
        return this.reps;
    }

    @Override
    public String toString(){
        return Double.toString(this.getWeight()) + " kg X " + Integer.toString(this.getReps());
    }
}