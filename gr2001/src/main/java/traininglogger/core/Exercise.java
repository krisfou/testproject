package traininglogger.core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Exercise implements Iterable<Set> {

    private String name;
    private List<Set> sets = new ArrayList<Set>();

    public Exercise(String name, Set... sets) { // OBS: Dette tillater å gi INGEN Set som argument
        this.name = name; // Bør ikke vær null, men hvem skal ta seg av det?
        for (Set set : sets) {
            this.sets.add(set);
        }
    }

    public void addSet(Set set){ // Lagt til for å gjøre ExerciseDeserializer enklere å skrive. God grunn? Endre?
        this.sets.add(set);      // Viste seg også å være nyttig i SessionController 
    }

    public String getName() {
        return this.name;
    }

    @Override
    public Iterator<Set> iterator() {
        return sets.iterator();
    }

    @Override
    public String toString(){
        String asString = this.getName() + ": \n \n";
        for (Set set : this){
            asString += set.toString() + "\n";
        }
        return asString;
    }
}