package traininglogger.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import traininglogger.core.Exercise;
import traininglogger.core.Session;
import traininglogger.core.Set;

public class SessionController {

    private Session session;
    private LogController logController;

    @FXML
    TextField nameTextField;

    @FXML
    VBox addSetVbox;

    @FXML
    HBox addSetHbox;

    @FXML
    TextField weightTextField;

    @FXML
    TextField repsTextField;

    @FXML
    Button addExerciseButton;

    public SessionController() {
        this.session = new Session();
    }

    public void setLogController(LogController logController){
        this.logController = logController;
    }

    private List<Set> sets = new ArrayList<Set>();

    @FXML
    private void addSetButtonHandler() {
        double weight = Double.parseDouble(weightTextField.getText());
        int reps = Integer.parseInt(repsTextField.getText());
        Set set = new Set(weight, reps);
        this.sets.add(set);
        addHboxToVbox();
        weightTextField.setText("");
        repsTextField.setText("");
    }

    private void addHboxToVbox() {
        FXMLLoader loader = new FXMLLoader();
        try {
            Node node = loader.load(getClass().getResource("HboxTemplate.fxml").openStream());
            addSetVbox.getChildren().add(addSetVbox.getChildren().size() - 1, node);
            HboxTemplateController controller = (HboxTemplateController) loader.getController();
            controller.setTextField(weightTextField.getText(), repsTextField.getText());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void addExerciseButtonHandler() {
        Exercise exercise = new Exercise(nameTextField.getText());
        addExerciseToSessionBox(exercise);
    }

    @FXML
    VBox sessionBox;

    private void addExerciseToSessionBox(Exercise exercise) {
        VBox vBox = new VBox();
        Label description = new Label();
        String setsAsString = "";
        for (Set set : this.sets) {
            exercise.addSet(set);
            setsAsString += set.toString() + "\n";
        }
        description.setText(setsAsString);
        vBox.getChildren().add(description);
        TitledPane titledPane = new TitledPane(exercise.getName(), vBox);
        titledPane.setAlignment(Pos.CENTER_LEFT);
        titledPane.setExpanded(false);
        this.sessionBox.getChildren().add(titledPane);

        // Rydder opp:
        // Gjør klar for ny øvelse:
        nameTextField.setText("");
        // Fjerne alle elementer i addSetVbox bortsett fra det første:
        int length = addSetVbox.getChildren().size() - 1;
        for (int i = 0; i < length; i++){
            addSetVbox.getChildren().remove(0);
        }
        this.sets = new ArrayList<Set>();

        // Legg til fullførte øvelse i økta:
        this.session.addExercise(exercise);
    }

    @FXML
    Button addSessionButton;

    public void addSessionButtonHandler(){
        this.sessionBox.getChildren().clear();
        this.logController.addSessionToSessionLog(this.session);
        this.session = new Session();
    }
}