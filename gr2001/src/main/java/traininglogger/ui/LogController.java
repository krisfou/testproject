package traininglogger.ui;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.fasterxml.jackson.databind.ObjectMapper;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.VBox;
import traininglogger.core.Exercise;
import traininglogger.core.Session;
import traininglogger.core.SessionLog;
import traininglogger.json.TrainingLoggerModule;

public class LogController {

    // En instans av Controller-klassen som er oppgitt i tilhørende fxml-fil vil
    // initialiseres
    // under FXMLLoader.load()-operasjonen.
    // Dette skjer FØR hele fxml-fila er bearbeidet, så på dette tidspunktet kan man
    // IKKE referere til JavaFX-elementer.
    // En følge av dette er at man i Controlleren setter opp domenedata i
    // konstruktøren, men at man venter med å
    // koble disse til JavaFX-elementene. Dette må gjøres i Controllerens
    // initialize()-metode. Denne metoden kjøres nemlig ETTER
    // at fxml-fila er ferdig bearbeidet.

    // Edit: Siden vi nå har lagt String-objekter i FXML-fila, så må vi utsette å
    // opprette domene-data også til initialize-metoden.

    @FXML
    public void initialize() {
        // Kobler domenedata til JavaFX-elementer:
        initializeSessionLog();
        for (Session session : this.sessionLog) {
            addSessionToLogBox(session);
        }
    }

    @FXML
    String userSessionLogPath;

    @FXML
    String sampleSessionLogResource;

    private final static String sessionLogWithTwoSessions = "{\"sessions\":[{\"date\":\"24/10/2020 13:46\",\"exercises\":[{\"name\":\"Benkpress\",\"sets\":[{\"weight\":100.0,\"reps\":5},{\"weight\":97.5,\"reps\":5},{\"weight\":87.5,\"reps\":5}]},{\"name\":\"Knebøy\",\"sets\":[{\"weight\":130.0,\"reps\":8},{\"weight\":127.5,\"reps\":8},{\"weight\":125.0,\"reps\":7}]}]},{\"date\":\"24/10/2020 13:46\",\"exercises\":[{\"name\":\"Markløft\",\"sets\":[{\"weight\":120.0,\"reps\":8},{\"weight\":97.5,\"reps\":10},{\"weight\":87.5,\"reps\":15}]},{\"name\":\"Bulgarsk utfall\",\"sets\":[{\"weight\":150.0,\"reps\":4},{\"weight\":127.5,\"reps\":10},{\"weight\":125.0,\"reps\":12}]}]}]}";
    private ObjectMapper mapper = new ObjectMapper();
    private SessionLog sessionLog;

    public void initializeSessionLog() {
        // Setter opp domenedata:
        this.mapper.registerModule(new TrainingLoggerModule());
        Reader reader = null;
        // Prøv å lese lagret fil fra brukerens hjemmeområde:
        if (userSessionLogPath != null) {
            try {
                reader = new FileReader(Paths.get(System.getProperty("user.home"), userSessionLogPath).toFile(),
                        StandardCharsets.UTF_8);
            } catch (IOException ioex) {
                System.err.println(
                        "Fant ingen " + userSessionLogPath + " på hjemmeområdet, prøver eksempelfil i stedet.");
            }
        }
        if (reader == null && sampleSessionLogResource != null) {
            // Prøv å bruke sample-sessionlog.json fra resources mappa i stedet:
            URL url = getClass().getResource(sampleSessionLogResource);
            if (url != null) {
                try {
                    reader = new InputStreamReader(url.openStream(), StandardCharsets.UTF_8);
                } catch (IOException e) {
                    System.err.println("Kunne ikke lese innebygget " + sampleSessionLogResource);
                }
            } else {
                System.err.println("Fant ikke innebygget " + sampleSessionLogResource
                        + ". Bruker hardkodet json-streng i stedet.");
            }
        }
        if (reader == null) {
            // bruk hardkodet json-streng:
            reader = new StringReader(sessionLogWithTwoSessions);
        }
        try {
            this.sessionLog = mapper.readValue(reader, SessionLog.class);
        } catch (IOException e) {
            System.err.println("Ingenting funka, du må lage din egen Logg!");
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                // skjer ikke
            }
        }
    }

    @FXML
    VBox logBox;

    private void addSessionToLogBox(Session session) {
        VBox vBox = new VBox();
        Label description = new Label();
        String exercisesAsString = "";
        for (Exercise e : session) {
            exercisesAsString += e.toString() + "\n";
        }
        description.setText(exercisesAsString);
        vBox.getChildren().add(description);
        TitledPane titledPane = new TitledPane(session.getDate(), vBox);
        titledPane.setAlignment(Pos.CENTER_LEFT);
        titledPane.setExpanded(false);
        this.logBox.getChildren().add(0, titledPane);
    }

    public void addSessionToSessionLog(Session session) {
        // Her burde egentlig SessionLogen være observerbar slik at Controlleren
        // automatisk fikk beskjed når den endret seg...
        this.sessionLog.addSession(session);
        addSessionToLogBox(session);
        saveSessionLog();
    }

    private void saveSessionLog() {
        if (userSessionLogPath != null) {
            Path path = Paths.get(System.getProperty("user.home"), userSessionLogPath);
            try {
                Writer writer = new FileWriter(path.toFile(), StandardCharsets.UTF_8);
                this.mapper.writerWithDefaultPrettyPrinter().writeValue(writer, this.sessionLog);
                System.out.println("Ny logg skrevet til fil"); //DEBUG
            } catch (IOException e) {
                System.err.println("Fikk ikke skrevet til sessionlog.json på hjemmeområdet.");
            }
        }
    }

}