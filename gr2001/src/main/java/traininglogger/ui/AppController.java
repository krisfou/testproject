package traininglogger.ui;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.AnchorPane;

public class AppController {

    @FXML
    AnchorPane appView;

    private Parent logViewNode;
    private Parent sessionViewNode;

    public void initialize() throws IOException {
        FXMLLoader logLoader = new FXMLLoader();
        this.logViewNode = logLoader.load(getClass().getResource("Log.fxml").openStream());
        LogController logController = logLoader.getController();
        FXMLLoader sessionLoader = new FXMLLoader();
        this.sessionViewNode = sessionLoader.load(getClass().getResource("Session.fxml").openStream());
        ((SessionController)sessionLoader.getController()).setLogController(logController);; // hack...
    }

    @FXML
    public void handleLogButtonAction() {
        this.appView.getChildren().clear();
        this.appView.getChildren().add(this.logViewNode);
    }

    @FXML
    public void handleNewSessionButtonAction() {
        this.appView.getChildren().clear();
        this.appView.getChildren().add(this.sessionViewNode);
    }
}