package traininglogger.json;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.DoubleNode;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import traininglogger.core.Set;

public class SetDeserializer extends JsonDeserializer<Set> {

    /* format:
     * { 
     * "weight" :87,5, 
     * "reps" :5
     * }
     */

    @Override
    public Set deserialize(JsonParser parser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        TreeNode treeNode = parser.getCodec().readTree(parser);
        return deserialize((JsonNode) treeNode);

    }

    public Set deserialize(JsonNode jsonNode) {
        if (jsonNode instanceof ObjectNode) {
            ObjectNode objectNode = (ObjectNode) jsonNode;
            JsonNode weightNode = objectNode.get("weight");
            JsonNode repsNode = objectNode.get("reps");
            if (weightNode instanceof DoubleNode && repsNode instanceof IntNode) {
                double weight = ((DoubleNode) weightNode).asDouble();
                int reps = ((IntNode) repsNode).asInt();
                Set set = new Set(weight, reps);
                return set;
            }
        }
        return null;
    }
}