package traininglogger.json;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import traininglogger.core.Set;

public class SetSerializer extends JsonSerializer<Set> {

    /* format:
     * { 
     * "weight" :87,5, 
     * "reps" :5
     * }
     */

    @Override
    public void serialize(Set set, JsonGenerator jGen, SerializerProvider serializerProvider) throws IOException {
        jGen.writeStartObject();
        jGen.writeNumberField("weight", set.getWeight());
        jGen.writeNumberField("reps", set.getReps());
        jGen.writeEndObject();
    }
}