package traininglogger.json;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import traininglogger.core.Exercise;
import traininglogger.core.Set;

public class ExerciseSerializer extends JsonSerializer<Exercise> {

    /* format:
     * {
     * "name" : "...",
     * "sets" :[...]
     * }
     */

    @Override
    public void serialize(Exercise exercise, JsonGenerator jGen, SerializerProvider serializerProvider) throws IOException {
        jGen.writeStartObject();
        jGen.writeStringField("name", exercise.getName());
        jGen.writeArrayFieldStart("sets");
        for (Set set : exercise){
            jGen.writeObject(set);
        }
        jGen.writeEndArray();
        jGen.writeEndObject();
    }
}