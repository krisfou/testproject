package traininglogger.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import traininglogger.core.Exercise;
import traininglogger.core.Session;
import traininglogger.core.SessionLog;
import traininglogger.core.Set;

@SuppressWarnings("serial")
public class TrainingLoggerModule extends SimpleModule {
    private static final String NAME = "TrainingLoggerModule";

    public TrainingLoggerModule() {
        super(NAME, Version.unknownVersion());
        addSerializer(Set.class, new SetSerializer());
        addDeserializer(Set.class, new SetDeserializer());
        addSerializer(Exercise.class, new ExerciseSerializer());
        addDeserializer(Exercise.class, new ExerciseDeserializer());
        addSerializer(Session.class, new SessionSerializer());
        addDeserializer(Session.class, new SessionDeserializer());
        addSerializer(SessionLog.class, new SessionLogSerializer());
        addDeserializer(SessionLog.class, new SessionLogDeserializer());
    }

    // Uformell testing:
    public static void main(String[] args) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new TrainingLoggerModule());

        // Session1:
        Set set0 = new Set(100, 5);
        Set set1 = new Set(97.5, 5);
        Set set2 = new Set(87.5, 5);

        Set[] sets1 = new Set[3];
        sets1[0] = set0;
        sets1[1] = set1;
        sets1[2] = set2;

        Exercise exercise1 = new Exercise("Benkpress", sets1);

        Session session1 = new Session();
        session1.addExercise(exercise1);

        Set set3 = new Set(130, 8);
        Set set4 = new Set(127.5, 8);
        Set set5 = new Set(125, 7);

        Set[] sets2 = new Set[3];
        sets2[0] = set3;
        sets2[1] = set4;
        sets2[2] = set5;

        Exercise exercise2 = new Exercise("Knebøy", sets2);
        session1.addExercise(exercise2);

        // Session2:

        Set set6 = new Set(120, 8);
        Set set7 = new Set(97.5, 10);
        Set set8 = new Set(87.5, 15);

        Set[] sets3 = new Set[3];
        sets3[0] = set6;
        sets3[1] = set7;
        sets3[2] = set8;

        Exercise exercise3 = new Exercise("Markløft", sets3);

        Session session2 = new Session();
        session2.addExercise(exercise3);

        Set set9 = new Set(150, 4);
        Set set10 = new Set(127.5, 10);
        Set set11 = new Set(125, 12);

        Set[] sets4 = new Set[3];
        sets4[0] = set9;
        sets4[1] = set10;
        sets4[2] = set11;

        Exercise exercise4 = new Exercise("Bulgarsk utfall", sets4);
        session2.addExercise(exercise4);

        // SessionLog:

        SessionLog sessionLog = new SessionLog();
        sessionLog.addSession(session1);
        sessionLog.addSession(session2);

        try {
            String sessionLogAsJsonString = mapper.writeValueAsString(sessionLog); // fra objekt til json-streng
            System.out.println(
                    "----------------------------------------------------------------------------------------------------------------------------------------");
            System.out.println(sessionLogAsJsonString); // funker!
            System.out.println(
                    "----------------------------------------------------------------------------------------------------------------------------------------");

            SessionLog sessionLogFromJsonString = mapper.readValue(sessionLogAsJsonString, SessionLog.class); // fra
                                                                                                              // json-streng
                                                                                                              // tilbake
                                                                                                              // til
                                                                                                              // objekt

            for (Session session : sessionLogFromJsonString) {
                System.out.println(session.getDate());
                for (Exercise exercise : session) {
                    System.out.println(exercise.toString());
                }
            }
        } catch (JsonProcessingException e) {
            System.err.println("Virket ikke.");
            e.printStackTrace();
        }

        // Funker fint! :-) Utgangspunkt for en test av TrainingLoggerModule og
        // indirekte alle Serializere/Deserializere!
    }
}