package traininglogger.json;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;

import traininglogger.core.Exercise;
import traininglogger.core.Set;

public class ExerciseDeserializer extends JsonDeserializer<Exercise> {

    private SetDeserializer setDeserializer = new SetDeserializer();

    /*
     * format: { "name" : "...", "sets" :[...] }
     */

    @Override
    public Exercise deserialize(JsonParser parser, DeserializationContext ctxt)
            throws IOException, JsonProcessingException {
        TreeNode treeNode = parser.getCodec().readTree(parser);
        return deserialize((JsonNode) treeNode);
    }

    public Exercise deserialize(JsonNode jsonNode) {
        if (jsonNode instanceof ObjectNode) {
            ObjectNode objectNode = (ObjectNode) jsonNode;
            JsonNode nameNode = objectNode.get("name");
            JsonNode setsNode = objectNode.get("sets");
            if (nameNode instanceof TextNode && setsNode instanceof ArrayNode) {
                String name = ((TextNode) nameNode).asText();
                ArrayNode sets = (ArrayNode) setsNode;
                Exercise exercise = new Exercise(name);
                for (JsonNode element : sets) {
                    Set set = this.setDeserializer.deserialize(element);
                    if (set != null) {
                        exercise.addSet(set);
                    }
                }
                return exercise;
            }
        }
        return null;
    }
}