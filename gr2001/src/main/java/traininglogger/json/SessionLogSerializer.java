package traininglogger.json;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import traininglogger.core.Session;
import traininglogger.core.SessionLog;

public class SessionLogSerializer extends JsonSerializer<SessionLog> {

    /* format:
     * {
     * "sessions" : [...]
     * }
     */

    @Override
    public void serialize(SessionLog sessionLog, JsonGenerator jGen, SerializerProvider serializerProvider) throws IOException {
        jGen.writeStartObject();
        jGen.writeArrayFieldStart("sessions");
        for (Session session : sessionLog){
            jGen.writeObject(session);
        }
        jGen.writeEndArray();
        jGen.writeEndObject();
    }
}