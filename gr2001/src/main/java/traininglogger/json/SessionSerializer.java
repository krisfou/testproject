package traininglogger.json;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import traininglogger.core.Exercise;
import traininglogger.core.Session;

public class SessionSerializer extends JsonSerializer<Session> {

    /* format:
     * {
     * "date" : "...",
     * "exercises" :[...]
     * }
     */

    @Override
    public void serialize(Session session, JsonGenerator jGen, SerializerProvider serializerProvider) throws IOException {
        jGen.writeStartObject();
        jGen.writeStringField("date", session.getDate());
        jGen.writeArrayFieldStart("exercises");
        for (Exercise exercise : session){
            jGen.writeObject(exercise);
        }
        jGen.writeEndArray();
        jGen.writeEndObject();
    }
}