package traininglogger.json;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;

import traininglogger.core.Exercise;
import traininglogger.core.Session;

public class SessionDeserializer extends JsonDeserializer<Session> {

    private ExerciseDeserializer exerciseDeserializer = new ExerciseDeserializer();

    /*
     * format: { "name" : "...", "sets" :[...] }
     */

    @Override
    public Session deserialize(JsonParser parser, DeserializationContext ctxt)
            throws IOException, JsonProcessingException {
        TreeNode treeNode = parser.getCodec().readTree(parser);
        return deserialize((JsonNode) treeNode);
    }

    public Session deserialize(JsonNode jsonNode) {
        if (jsonNode instanceof ObjectNode) {
            ObjectNode objectNode = (ObjectNode) jsonNode;
            JsonNode dateNode = objectNode.get("date");
            JsonNode exercisesNode = objectNode.get("exercises");
            if (dateNode instanceof TextNode && exercisesNode instanceof ArrayNode) {
                String date = ((TextNode) dateNode).asText();
                ArrayNode exercises = (ArrayNode) exercisesNode;
                Session session = new Session(date);
                for (JsonNode element : exercises) {
                    Exercise exercise = this.exerciseDeserializer.deserialize(element);
                    if (exercise != null) {
                        session.addExercise(exercise);
                    }
                }
                return session;
            }
        }
        return null;
    }
}